const express = require("express");
const Kavenegar = require("kavenegar");
const gpc = require("generate-pincode");
const httpStatus = require("http-status");

const Constants = require("../helpers/constants");
const Errors = require("../helpers/errors");

const {
  User,
  validateRgister,
  validateActivationCode,
  validateSaveUserInformation
} = require("../models/user");

const router = express.Router();

const kavenegarApi = Kavenegar.KavenegarApi({
  apikey: Constants.SMS_API_KEY
});

router.post("/getActivationCode", async (request, response) => {
  try {
    const { error } = validateRgister(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({ phoneNumber: request.body.phoneNumber });

    if (!user) {
      user = new User({
        phoneNumber: request.body.phoneNumber,
        code: gpc(8),
        activationCode: gpc(6)
      });
    } else {
      user.activationCode = gpc(6);
    }

    user = await user.save();

    if (Constants.SMS_IS_AVAILABLE) {
      await kavenegarApi.VerifyLookup({
        receptor: user.phoneNumber,
        token: user.activationCode,
        template: Constants.SMS_TEMPLATE_VALIDATION_CODE
      });
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/login", async (request, response) => {
  try {
    const { error } = validateActivationCode(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    const user = await User.findOne({
      phoneNumber: request.body.phoneNumber,
      activationCode: request.body.activationCode
    });

    if (!user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_ACTIVATION_CODE });
    }

    user.token = user.token = await user.generateAuthToken();

    response.status(httpStatus.OK).json(user);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/saveUserInformation", async (request, response) => {
  try {
    const { error } = validateSaveUserInformation(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findById(request.body._id);

    if (!user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_ACTIVATION_CODE });
    }

    user.firstName = request.body.firstName;
    user.lastName = request.body.lastName;
    user.email = request.body.email;

    user = await user.save();

    response.status(httpStatus.OK).json(user);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

module.exports = router;
