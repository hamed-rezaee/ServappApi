const Joi = require("joi");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const Constants = require("../helpers/constants");

const schema = mongoose.Schema({
  firstName: { type: String, unique: false, maxlength: 64, required: false },
  lastName: { type: String, unique: false, maxlength: 64, required: false },
  phoneNumber: { type: String, unique: true, maxlength: 11, required: true },
  email: { type: String, unique: false, maxlength: 64, required: false },
  code: { type: String, unique: true, maxlength: 8, required: false },
  activationCode: { type: String, unique: true, maxlength: 6, required: false },
  token: { type: String, required: false }
});

schema.methods.generateAuthToken = async function() {
  return jwt.sign({ userId: this._id }, Constants.JWT_PRIVATE_KEY, { expiresIn: "28d" });
};

const Model = mongoose.model("User", schema);

function validateRgister(request) {
  const schema = {
    phoneNumber: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

function validateActivationCode(request) {
  const schema = {
    phoneNumber: Joi.string().required(),
    activationCode: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

function validateSaveUserInformation(request) {
  const schema = {
    _id: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

module.exports.User = Model;
module.exports.validateRgister = validateRgister;
module.exports.validateActivationCode = validateActivationCode;
module.exports.validateSaveUserInformation = validateSaveUserInformation;
