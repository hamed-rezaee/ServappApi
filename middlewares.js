const status = require("http-status");

module.exports.cors = (request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "*");
  response.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

  next();
};

module.exports.resourceException = (request, response, next) => {
  const error = new Error();

  error.status = status.NOT_FOUND;
  error.message = "not found.";

  next(error);
};

module.exports.exception = (error, request, response, next) => {
  response.status(error.status || status.INTERNAL_SERVER_ERROR).json({
    message: error.message
  });
};
