const morgan = require("morgan");
const express = require("express");
const mongoose = require("mongoose");

const Constants = require("./api/helpers/constants");
const { cors, resourceException, exception } = require("./middlewares");

const authenticationRoutes = require("./api/routes/authentications");

if (!Constants.JWT_PRIVATE_KEY) {
  console.error("FATAL ERROR: JWT_PRIVATE_KEY is not defiend.");

  process.exit(1);
}

const app = express();

app.use(morgan("dev"));
app.use(express.json());
app.use(cors);

app.use(`${Constants.BASE_URL}/authentications`, authenticationRoutes);

app.use(resourceException);
app.use(exception);

app.listen(Constants.PORT, () => {
  console.log(`listening on port ${Constants.PORT}...`);
});

mongoose
  .connect(Constants.DATABASE_URL, {
    useNewUrlParser: true,
    useCreateIndex: true
  })
  .then(() => console.log("conected to mongodb..."))
  .catch(() => console.log("could not connect to mongodb."));
